package com.particular;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebParseUnicodeApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebParseUnicodeApplication.class, args);
	}

}
